# Anti Flicker #

This anti flicker module can be use for A/B testing to prevent flickering on loading variation.

### How do I get set up? ###

Import from specific directory.

```jsx
import AntiFlicker from './[DIR]/anti-flicker';
```

Initialize with delay (default of 4s) on page load before display on page content.

```jsx
const antiFlicker = new AntiFlicker()
```

Param @revealTime

Initialize with delay (1s) on page load before display on page content.

```jsx
const antiFlicker = new AntiFlicker(1000)
```

Add style to the page.

```jsx
antiFlicker.style()
```

Hide all content in the page.

```jsx
antiFlicker.hide()
```

Show all content in the page with delay.

```jsx
antiFlicker.show()
```

Combination of hide,s how and style methods.

```jsx
antiFlicker.init()
```