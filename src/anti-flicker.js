const id =  'tc-async-anti-flicker'

const className = 'tc-async-hide'

const style = '.tc-async-hide { opacity: 0 !important; visibility: hidden !important; }'

export default {

    data() {
        
        return {}

    },

    get $html() {

        return document.querySelector('html')

    },

    get $head() {

        return document.querySelector('head')

    },

    get $style() {
        
        return document.createElement('style')

    },

    get delay() {

        return this._delay

    },

    set delay(value) {

        this._delay = value

    },

    style() {

        const $style = this.$style

        if ( this.$head && !document.querySelector(`#${ id }`) ) {

            $style.innerHTML = style

            $style.id = id

            this.$head.append(
                $style
            )

        }

    },

    hide() {

        this.$html.classList.add(className)

    },

    show() {

        setTimeout ( () => {
          
            this.$html.classList.remove(className)

        }, this.delay || 4000)

    },

    mounted( delay ) {

        this.delay = delay

        this.style()

        this.hide()
        
        this.show()

    }

}