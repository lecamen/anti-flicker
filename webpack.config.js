const path = require('path');

module.exports = {
    mode: 'production',
    entry: './src/test.js',
    watch: false,
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    {
                        loader: 'css-loader',
                        options: {
                        }
                    }
                ],
            }
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'test.js',
    }
};
